/* Primeiro passo é criar um array de objetos que serão listados na tabela*/
//criando array
var alunos = new Array();
//Contador que ajudará a posicionar e pegar elementos pela posição
var cont = 0;

//função para validar o cpf
$(document).ready(function () {
    $("#returnMsg").hide();
    var $cpf = $("#cpf");
    $cpf.mask('000.000.000-00', {reverse: true});
});

//Construtor do Objeto que será inserido no array
function Aluno(id, nome, curso, disciplina, cpf, sexo, email){
    this.ID = id;
    this.Nome = nome;
    this.Curso = curso;
    this.Disciplina = disciplina;
    this.CPF = cpf;
    this.Sexo = sexo;
    this.Email = email;
}

//Validações sempre pegando os valores pelo ID da tags
function validar(){
var nome = document.getElementById("nome");
var curso = document.getElementById("curso");
var disciplina = document.getElementById("disciplina");
var cpf = document.getElementById("cpf");
var email = document.getElementById("email");
var sexo = null;

//Validação geral se um dos elementos está vazio já devolve mensagem avisando para terminar o preenchimento
if ((nome.value === "")&&(curso.value === "")&&(disciplina.value === "")&&(cpf.value === "")&&(email.value === "")&&
        (document.getElementById("rd1").checked === false) && (document.getElementById("rd2").checked === false)) {
        nome.setAttribute("class","textboxError");
        curso.setAttribute("class","textboxError");
        disciplina.setAttribute("class","textboxError");
        cpf.setAttribute("class","textboxError");
        email.setAttribute("class","textboxError");
        document.getElementById("rd1span").setAttribute("class","checkmarkError");
        document.getElementById("rd2span").setAttribute("class","checkmarkError");
        nome.select();
        msgReturn("Preencha o Formulário!",1);
} else {
    if (validarNome(nome)){
        nome.setAttribute("class","textboxOK");
            if (validarCurso(curso)){
                curso.setAttribute("class","textboxOK");
                    if (validarDisciplina(disciplina)){
                        disciplina.setAttribute("class","textboxOK");
                            if (validarCPF(cpf)) {
                                cpf.setAttribute("class","textboxOK");
                                    if (document.getElementById("rd1").checked === true) {
                                            sexo = "M";
                                    } else if (document.getElementById("rd2").checked === true) {
                                            sexo = "F";
                                    } else {
                                            document.getElementById("rd1span").setAttribute("class","checkmarkError");
                                            document.getElementById("rd2span").setAttribute("class","checkmarkError");
                                            msgReturn("Por favor, informe o sexo.",1);
                                    }
                                    if (sexo !== null) {
                                            document.getElementById("rd1span").setAttribute("class","checkmarkOK");
                                            document.getElementById("rd2span").setAttribute("class","checkmarkOK");
                                    if (validarEmail(email)){
                                            nome.setAttribute("class","textboxOK");
                                            curso.setAttribute("class","textboxOK");
                                            disciplina.setAttribute("class","textboxOK");
                                            email.setAttribute("class","textboxOK");
                                            cpf.setAttribute("class","textboxOK");
                                            document.getElementById("rd1span").setAttribute("class","checkmarkOK");
                                            document.getElementById("rd2span").setAttribute("class","checkmarkOK");
                                            msgReturn("Formul&aacute;rio preenchido com sucesso.",2);

                                            //cria um objeto e cadastra no array
                                            montaTabela(nome, curso, disciplina, cpf, sexo, email);
                                            limpaForm();
                                }
                            }
                        }
                    }
                }
            }
        }
}

//classe responsável por montar a tabela com os elementos dos OBJETOS do Array
function montaTabela(nome, curso, disciplina, cpf, sexo, email){
    aluno = new Aluno(cont++,nome.value,curso.value,disciplina.value,cpf.value,sexo,email.value);
    alunos.push(aluno);

    var sb = new StringBuilder();

    sb.append('<table id="tabelaAluno" class="table table-striped">');
    sb.append('<thead>');
    sb.append('<tr>');
    sb.append('<th>View</th>');
    sb.append('<th>Editar</th>');
	sb.append('<th>Excluir</th>');
	sb.append('<th>Nome</th>');
    sb.append('<th>Curso</th>');
    sb.append('<th>Disciplina</th>');
    sb.append('<th>CPF</th>');
    sb.append('<th>Sexo</th>');
    sb.append('<th>E-mail</th>');
    sb.append('</tr>');
    sb.append('</thead>');
    sb.append('<tbody>');
	//laço para posiciona-los por linha e coluna
    for (var i=0; i<alunos.length; i++) {
        sb.append('<tr id="alunoLinha'+alunos[i].ID+'">');
        sb.append('<td><a href="javascript:abrirJanela('+alunos[i].ID+');"><img src="img/24px.svg" /></a></td>');
		sb.append('<td><a href="javascript:editarItem('+alunos[i].ID+');"><img src="img/editar.jpg" /></a></td>');
		sb.append('<td><a href="javascript:excluirItem('+alunos[i].ID+');"><img src="img/excluir.jpg" /></a></td>');
        sb.append('<td id="nome">'+alunos[i].Nome+'</td>');
        sb.append('<td id="curso">'+alunos[i].Curso+'</td>');
        sb.append('<td id="disciplina">'+alunos[i].Disciplina+'</td>');
        sb.append('<td id="cpf">'+alunos[i].CPF+'</td>');
        sb.append('<td id="sexo">'+alunos[i].Sexo+'</td>');
        sb.append('<td id="email">'+alunos[i].Email+'</td>');
        sb.append('</tr>');
    }
    sb.append('</tbody>');
    sb.append('</table>');
    
    document.getElementById("tabela").innerHTML = sb.toString();
}

//limpando o form para novo cadastro
function limpaForm(){    
    $('#nome').removeClass("textboxOK");
    $('#nome').addClass("textbox");
    document.getElementById('nome').value = "";
    $('#curso').removeClass("textboxOK");
    $('#curso').addClass("textbox");
    document.getElementById('curso').value = "";
    $('#disciplina').removeClass("textboxOK");
    $('#disciplina').addClass("textbox");
    document.getElementById('disciplina').value = "";
    $('#cpf').removeClass("textboxOK");
    $('#cpf').addClass("textbox");
    document.getElementById('cpf').value = "";
    $('#rd1span').removeClass("checkmarkOK");
    $('#rd1span').addClass("checkmark");
    document.getElementById('rd1span').checked = false;
    $('#rd2span').removeClass("checkmarkOK");
    $('#rd2span').addClass("checkmark");
    document.getElementById('rd2span').checked = false;
    $('#email').removeClass("textboxOK");
    $('#email').addClass("textbox");
    document.getElementById('email').value = "";
}

function StringBuilder(value) {
    this.strings = new Array();
    this.append(value);
}

StringBuilder.prototype.append = function (value) {
    if (value) {
        this.strings.push(value);
    }
}

StringBuilder.prototype.clear = function () {
    this.strings.length = 0;
}

StringBuilder.prototype.toString = function () {
    return this.strings.join("");
}

function excluirItem(item){
	//Aqui recebe o indice da linha a ser excluída
	item++;
	//Exclui a linha da tabela
	document.getElementById("tabelaAluno").deleteRow(item);
	//Remove o Objeto pelo índice do array de objetos
	alunos.splice(aluno, item);
	//Acerta o contador para não dar problemas nas próximas inserções ou remoções
	cont = alunos.length;
}

//Função para imprimir em pdf com o elemento selecionado no clique do mouse <a href abrirJanela()>
function abrirJanela(id){ 
	var novonome = alunos[id].Nome;
	var novocurso = alunos[id].Curso;
	var novodisciplina = alunos[id].Disciplina;
	var novocpf = alunos[id].CPF;
	var novosexo = alunos[id].Sexo;
	var novoemail = alunos[id].Email;

	var style = "<style>";
	style = style + "table {width: 100%;font: 20px Calibri;}";
	style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
	style = style + "padding: 2px 3px;text-align: center;}";
	style = style + "</style>";
	// CRIA UM OBJETO WINDOW
	var win = window.open('', '', 'height=1000,width=1000');
	win.document.write('<!DOCTYPE html><html lang="pt-br"><meta charset="UTF-8"><title>Dados Aluno Matriculado</title><link rel="stylesheet" href="css/style.css" type="text/css" media="all"><link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all"><script type="text/javascript" src="js/jquery.min.js"></script><script type="text/javascript" src="js/bootstrap.min.js"></script><script type="text/javascript" src="js/jquery.mask.js"></script>      <script type="text/javascript" src="js/jsFunctions.js" charset="UTF-8"></script>');
	win.document.write(style);                    	// INCLUI UM ESTILO NA TAB HEAD
	win.document.write('</head>');
	win.document.write('<body><header><div class="container"><h1>Fundamentos de Programação para a Web</h1></div>');
	win.document.write('<img src="../furg.jpg" class="top"/>');
	win.document.write('<form id="form1"><div class="rowLabel"><label>Nome: </label>');   // O CONTEUDO DA TABELA DENTRO DA TAG BODY
	win.document.write(novonome);
	win.document.write('</div><div class="rowLabel"><label>Curso: </label>');
	win.document.write(novocurso);
	win.document.write('</div><div class="rowLabel"><label>Disciplina: </label>');
	win.document.write(novodisciplina);
	win.document.write('</div><div class="rowLabel"><label>CPF: </label>');
	win.document.write(novocpf);
	win.document.write('</div><div class="rowLabel"><label>Sexo: </label>');
	win.document.write(novosexo);
	win.document.write('</div><div class="rowLabel"><label>Email: </label>');
	win.document.write(novoemail);
	win.document.write('</div>');
	win.document.write('</form><footer><p class="container">Fundamentos de Programação Para Internet</p><address>Douglas Ristow <br/>Thiarles Rocha Carneiro da Cunha</address></footer></body></html>');
	win.document.close(); 	                       // FECHA A JANELA
	win.print();                                   // IMPRIME O CONTEUDO
}

function validarNome(nome){
    regex = /\b[A-Za-zÀ-ú][A-Za-zÀ-ú]+,?\s[A-Za-zÀ-ú][A-Za-zÀ-ú]{1,12}\b/gi;

    if (regex.test(nome.value)) {
        return true;
    } else {
        nome.select();
        nome.setAttribute("class","textboxError");
        msgReturn("Por favor, preencha o seu nome.",3);
    return false;
    }
}

function validarCurso(curso){
    regex = /\b[A-Za-zÀ-ú][A-Za-zÀ-ú]+,?\s[A-Za-zÀ-ú][A-Za-zÀ-ú]{1,12}\b/gi;

    if (regex.test(curso.value)) {
        return true;
    } else {
        curso.select();
        curso.setAttribute("class","textboxError");
        msgReturn("Por favor, preencha o nome do curso.",3);
    return false;
    }
}

function validarDisciplina(disciplina){
    regex = /\b[A-Za-zÀ-ú][A-Za-zÀ-ú]+,?\s[A-Za-zÀ-ú][A-Za-zÀ-ú]{1,12}\b/gi;

    if (regex.test(disciplina.value)) {
        return true;
    } else {
        disciplina.select();
        disciplina.setAttribute("class","textboxError");
        msgReturn("Por favor, preencha o nome da disciplina.",3);
    return false;
    }
}

function validarEmail(email){
    regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (regex.test(email.value)) {
        return true;
    } else {
        email.select();
        email.setAttribute("class","textboxError");
        msgReturn("Por favor, preencha um e-mail v&aacute;lido.",3);
    return false;
    }
}

function validarCPF(cpf){
    regex = /^[\d]{3}\.[\d]{3}\.[\d]{3}\-[\d]{2}$/;

    if (regex.test(cpf.value)) {
        return true;
    } else {
        cpf.select();
        msgReturn("CPF Inv&aacute;lido. Exemplo v&aacute;lido: 000.000.000-00.",3);
    return false;
    }
}

function msgReturn(msg, tipo){
    switch (tipo) {
        case 1:
            $('#returnMsg').removeClass("alert alert-success");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').addClass("alert alert-danger");
        break;
        case 2:
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').removeClass("alert alert-success");
            $('#returnMsg').addClass("alert alert-success");
        break;
        case 3:
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').removeClass("alert alert-success");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').addClass("alert alert-warning");
        break;
        default:
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').removeClass("alert alert-success");
            $('#returnMsg').addClass("alert alert-success");
        break;
    }

    $("#returnMsg").fadeTo(1, 1).removeClass('hidden');
    $("#returnMsg").show();

    if (msg !== "") {
        document.getElementById("msgReturn").innerHTML = msg;
    }

    window.setTimeout(function() {
    $("#returnMsg").fadeTo(500, 0).slideUp(500, function(){
        $("#returnMsg").addClass('hidden');
    });
    }, 4000);
}